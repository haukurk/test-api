﻿using System.Collections.Generic;
using System.Linq;
using common.DTOs;
using data.Repositories;

namespace services
{
    public class AuthorService
    {
        readonly IAuthorRepository _authorRepository;

        public AuthorService(IAuthorRepository authorRepository) {
            _authorRepository = authorRepository;
        }

        /// <summary>
        /// Gets the authors.
        /// </summary>
        /// <returns>The authors.</returns>
        public IEnumerable<AuthorDTO> GetAuthors() {

            var authorEntites = _authorRepository.GetAllAuthors();

            /* Simplest mapping logic ever done */
            var authorDTOs = authorEntites.Select(x => new AuthorDTO()
            {
                Name = x.HOFUNDUR,
                Email = x.POSTFANG
            });

            /* Do something else with Author? */

            return authorDTOs;
        }

    }
}
