﻿using System.Data;

namespace data
{
    public interface IDbFactory
    {
        IDbConnection GetConnection();
    }
}
