﻿using System.Collections.Generic;
using data.Entities;

namespace data.Repositories
{
    public interface IAuthorRepository
    {
        IEnumerable<Author> GetAllAuthors();
    }
}
