﻿using System;
using System.Collections.Generic;
using Dapper;
using data.Entities;

namespace data.Repositories
{
    public class AuthorRepository : IAuthorRepository
    {
        readonly IDbFactory _dbFactory;

        public AuthorRepository(IDbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        /// <summary>
        /// Get all articles from Dacoda.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Author> GetAllAuthors()
        {
            IEnumerable<Author> authors = null;

            using (var db = _dbFactory.GetConnection())
            {
                db.Open();

                string sql = @"SELECT HOFUNDUR, POSTFANG FROM NOTENDUR";
                authors = db.Query<Author>(sql, commandTimeout: 200);
            }

            return authors;
        }
    }
}
