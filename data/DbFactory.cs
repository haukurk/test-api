﻿using System.Data;
using System.Data.SqlClient;

namespace data
{
    public class DbFactory : IDbFactory
    {
        readonly string _connectionString;

        public DbFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        IDbConnection IDbFactory.GetConnection()
        {
            return new SqlConnection(_connectionString);
        }
    }
}
