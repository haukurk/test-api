﻿using System;
namespace common.DTOs
{
    public class AuthorDTO
    {
        public int id { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
    }
}
