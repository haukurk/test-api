﻿
using System.Collections.Generic;
using common.DTOs;
using data.Repositories;
using Microsoft.AspNetCore.Mvc;
using services;

namespace testapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        readonly AuthorService _authorService;

        public AuthorController(IAuthorRepository authorRepository) {
            _authorService = new AuthorService(authorRepository);
        }

        // /api/authors
        [HttpGet]
        [Route("authors")]
        public IEnumerable<AuthorDTO> GetAuthors() {

            /* Authentication missing, and authorization */

            var authors = _authorService.GetAuthors();

            return authors;

        }
    }
}
