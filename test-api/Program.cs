﻿using System;
using System.Reflection;
using Autofac;
using data;
using data.Repositories;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Autofac.Integration.WebApi;
using System.Web.Http.Dependencies;

namespace test_api 
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Utilize app config
            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .Build();

            // Register dependencies with IoC container.
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<DbFactory>().As<IDbFactory>()
                   .WithParameter("connectionString", configuration.GetConnectionString("MasterAuthorDb"));

            builder.RegisterType<AuthorRepository>().As<IAuthorRepository>();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
